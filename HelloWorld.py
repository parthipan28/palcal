import re
import calendar
from datetime import datetime, date, time, timedelta
from flask import Flask, render_template, request, jsonify, redirect, url_for
import dbConnect
import jwt
from functools import wraps

app = Flask(__name__)
app.config['SECRET_KEY'] = 'palcalSecretKey'
spentDB = []


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        print('Deco*****************')
        token = None
        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']
        if not token:
            return jsonify({'message': 'a valid token is missing'})
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'], algorithm='HS256')
            print(datetime.now(), datetime.fromtimestamp(data['exp']))
            if datetime.now() > datetime.fromtimestamp(data['exp']):
                return jsonify({'Auth_Status': '5001'})
            else:
                return jsonify({'message': 'token is valid'})
        except:
            return jsonify({'Auth_Status': '5002'})
            #return render_template('login.html', display_msg='Token Invalid')
        return f(*args, **kwargs)
    return decorator

@app.route('/', methods=['GET'])
def index():
    return render_template('login.html')

@app.route('/login', methods=['POST'])
def login():
    print(request.form)
    username = request.form.getlist('uname')[0]
    pwd = request.form.getlist('psw')[0]
    token=''
    try:
        con = dbConnect.DBConnect()
        db_result = con.validate_login(username)
        if db_result == None:
            display_msg="User Not Exist"
            return render_template('login.html',display_msg=display_msg)
        elif db_result[0] != str(pwd):
            display_msg = "Password, please retry"
            return render_template('login.html', display_msg=display_msg)
        else:
            display_msg = "Login sucess"
            print(display_msg)
            token = jwt.encode(
                        {'public_id': username,
                         'exp': datetime.utcnow() + timedelta(seconds=60)},
                        app.config['SECRET_KEY'], algorithm='HS256')
            print(token)
            x_token = token.decode('UTF-8')
            print('~~~~~~~~~~~~~~~~~~')
            print(x_token)
            #return jsonify({'token': token.decode('UTF-8')})
    except Exception as e:
        print(str(e))

    return redirect(url_for('home',token= x_token,userid=username))


@app.route('/profile', methods=['GET'])
def home():
    print(request.args['userid'])
    current_user = request.args['userid']
    token = request.args['token']
    connection(current_user)
    cal = calendar.Calendar()
    #cal.month(2020,10)
    #global spentDB
    dates = cal.itermonthdays2(datetime.today().year, datetime.today().month)
    alldates = cal.itermonthdates(datetime.today().year, datetime.today().month)
    spent = spentDB
    #spent = [['2020-09-28', 300], ['2020-10-15', 250], ['2020-10-31', 40]]
    val = [str(x) for x in alldates]
    listofdays = [[i, i[-2:], 0] for i in val]
    for sp in spent:
        for da in listofdays:
            if sp[0] == da[0]:
                da[2] = sp[1]

    print(listofdays)
    monthlist = []
    for i in range(0, len(listofdays), 7):
        wks = []
        for j in listofdays[i:i+7]:
            wks.append([int(j[1]), int(j[2]), j[0]])
        monthlist.append(wks)
    #print(monthlist)

##    cal = calendar.HTMLCalendar()
##    monthlist = cal.formatmonth(datetime.today().year, datetime.today().month)
##    print(monthlist)
    return render_template('index.html', currmon=monthlist,token="'"+token+"'")
#    return render_template('index.html')

@app.route('/nm', methods=['GET'])
def nextmonth():
    cal = calendar.Calendar()
    datepara = date.today()
    dates = cal.itermonthdays2(datetime.today().year, datetime.today().month)
    alldates = cal.itermonthdates(datetime.today().year, datetime.today().month)
    spent = spentDB
    #spent = [['2020-09-28', 300], ['2020-10-15', 250], ['2020-10-31', 40]]
    val = [str(x) for x in alldates]
    listofdays = [[i, i[-2:], 0] for i in val]
    for sp in spent:
        for da in listofdays:
            if sp[0] == da[0]:
                da[2] = sp[1]

    print(listofdays)
    monthlist = []
    for i in range(0, len(listofdays), 7):
        wks = []
        for j in listofdays[i:i+7]:
            wks.append([int(j[1]), int(j[2]), j[0]])
        monthlist.append(wks)
    print(monthlist)

##    cal = calendar.HTMLCalendar()
##    monthlist = cal.formatmonth(datetime.today().year, datetime.today().month)
##    print(monthlist)
    return render_template('index.html', currmon=monthlist)

@app.route('/month_update')
def month_update():
    passdate = request.args.get('passdate')
    print(passdate,passdate[:4],passdate[4:6])
    cal = calendar.Calendar()
    datepara = date(int(passdate[:4]), int(passdate[4:6]),1)
    dates = cal.itermonthdays2(datepara.year, datepara.month)
    alldates = cal.itermonthdates(datepara.year, datepara.month)
    spent = spentDB
    #spent = [['2020-09-28', 400], ['2020-10-15', 250], ['2020-10-31', 40]]
    val = [str(x) for x in alldates]
    listofdays = [[i, i[-2:], 0] for i in val]
    for sp in spent:
        for da in listofdays:
            if sp[0] == da[0]:
                da[2] = sp[1]

    print(listofdays)
    monthlist = []
    for i in range(0, len(listofdays), 7):
        wks = []
        for j in listofdays[i:i + 7]:
            wks.append([int(j[1]), int(j[2]), j[0]])
        monthlist.append(wks)
    #print(monthlist)
    return jsonify(currmon= monthlist)

@app.route('/authenticate',methods=['GET'])
@token_required
def authenticate():
    print('Authenticate')
    return None



def connection(current_user):
    global spentDB
    con = dbConnect.DBConnect()
    spentDB = con.retrieve_by_user(current_user)
    print(spentDB)


if __name__ == '__main__':
    #connection()

    # userpass = 'mysql+pymysql://root:root@'
    # basedir = 'localhost'
    # dbname = '/palcal'
    # dbname = dbname
    # app.config['SQLALCHEMY_DATABASE_URI'] = userpass + basedir + dbname
    # app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
    # db = SQLAlchemy(app)
    # results = db.engine.execute("SELECT * from USERS")
    # for x in results:
    #     print(x.userid)

    app.run()

